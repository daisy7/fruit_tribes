//index.js
Page({
  data: {
    searchValue:'',
    boards: [
      {
        'banner_img': "http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/banner/9aadcdb0-aaa8-4421-b1ec-fbdb55ffb38f.png",
        'banner_url':''
      },
      {
        'banner_img': "http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/banner/9aadcdb0-aaa8-4421-b1ec-fbdb55ffb38f.png",
        'banner_url': ''
      }
    ],
    nav_data: [
      {
        'nav_url': '',
        'nav_img': 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/nav_icon/aee897cf-bc31-4259-a0e5-295817b87274.png',
        'nav_text': '申请合作'
      },
      {
        'nav_url': '',
        'nav_img': 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/nav_icon/0d51dd57-2a65-4881-be37-6841e47c8cda.png',
        'nav_text': '采摘预约'
      },
      {
        'nav_url': '',
        'nav_img': 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/nav_icon/1671b527-8304-4280-9007-f9b860f81e42.png',
        'nav_text': '积分兑换'

      },
      {
        'nav_url': '',
        'nav_img': 'https://gitlab.com/daisy7/fruit_tribes/raw/front/FruitTribe/images/nav_icon/988c5e74-bafe-44c8-bdc8-5304d104e0c9.png',
        'nav_text': '优惠券'

      },
    ],
    indexData: {
      id: 1000001,
      data: [
        {
          name: 'star',
          title: '采摘预约',
          bannerSrc: 'https://gitlab.com/daisy7/fruit_tribes/raw/front/FruitTribe/images/misc/8970540b-8bf8-4db6-a333-5c67ff977dde.png',
          asBannerSrc: [],
          isShowBanner: true,
          child: [
            [
              {
                id: 1010101,
                index: 1,
                title: '[水蜜桃]',
                desc: ' 预计周二采果、空运.第 一时间保证新鲜，美味与新鲜同享',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E9%87%87%E6%91%98%E9%A2%84%E5%91%8A/80dd9606-7c65-43b1-b47f-67d5f01658c4.png'
              },
              {
                id: 1010102,
                index: 2,
                title: '[妃子笑]',
                desc: '新鲜荔枝采摘直播， 尽在6月17日，提前预约优鲜发货',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E9%87%87%E6%91%98%E9%A2%84%E5%91%8A/bf287de4-74c7-4c0f-88af-536d458f1df8.png'
              }
            ],
            [
              {
                id: 1010103,
                index: 3,
                title: '[网纹甜瓜]',
                desc: '预计6月20日发货， 健康香甜，支持团购，欲购从速',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E9%87%87%E6%91%98%E9%A2%84%E5%91%8A/d1e96814-a73d-4477-b1d4-c4575dc74b82.png'
              },
              {
                id: 1010104,
                index: 4,
                title: '[甜杏现货]',
                desc: '即采摘即发货，三斤 起卖支持团购',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E9%87%87%E6%91%98%E9%A2%84%E5%91%8A/62e869dc-2af5-4d20-b150-1d1fd2ac7378.png'
              }
            ],
            [
              {
                id: 1010105,
                index: 5,
                title: '[红心火龙果]',
                desc: '最后一批货物，味 美价廉，先定先得',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E9%87%87%E6%91%98%E9%A2%84%E5%91%8A/50c4e531-6121-4b79-894a-0768914e3127.png'
              },
              {
                id: 1010106,
                index: 6,
                title: '[精品莲雾] "',
                desc: '果园采摘，顺丰发货， 作水果的搬运工',
                price: '部小果',
                price_del: '',
                commodityType: 'discount',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E9%87%87%E6%91%98%E9%A2%84%E5%91%8A/f749affb-9b7a-4044-91ef-786e3c5d5b6b.png'
              }
            ]
          ]
        },
        {
          name: 'star',
          title: '私人定制',
          bannerSrc: 'https://gitlab.com/daisy7/fruit_tribes/raw/front/FruitTribe/images/misc/8970540b-8bf8-4db6-a333-5c67ff977dde.png',
          asBannerSrc: [],
          isShowBanner: true,
          child: [
            [
              {
                id: 1010101,
                index: 1,
                title: '[塑形健身]',
                desc: ' 完美搭配，你的私人 理疗顾问，搭配发货只为你的健康',
                price: '部小果',
                price_del: '',
                commodityType: '', 
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E6%9E%9C%E6%B1%81/612cda1b-e20b-4435-ba54-c9632bfaf253.png'
              },
              {
                id: 1010102,
                index: 2,
                title: '[塑形健身]',
                desc: '完美搭配，你的私人 理疗顾问，搭配发货只为你的健康',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E6%9E%9C%E6%B1%81/b36dd8d0-9d41-442c-8f71-a103b07e83b7.png'
              }
            ],
            [
              {
                id: 1010101,
                index: 1,
                title: '[塑形健身]',
                desc: ' 完美搭配，你的私人 理疗顾问，搭配发货只为你的健康',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E6%9E%9C%E6%B1%81/da3a6129-5608-4685-bae8-e5e3f880e324.png'
              },
              {
                id: 1010102,
                index: 2,
                title: '[塑形健身]',
                desc: '完美搭配，你的私人 理疗顾问，搭配发货只为你的健康',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E6%9E%9C%E6%B1%81/e4df49a1-3671-40c3-a31c-423afa9b0c91.png'
              }
            ]
          ]
        },
        {
          name: 'star',
          title: '营养减脂餐',
          bannerSrc: 'https://gitlab.com/daisy7/fruit_tribes/raw/front/FruitTribe/images/misc/8970540b-8bf8-4db6-a333-5c67ff977dde.png',
          asBannerSrc: [],
          isShowBanner: true,
          child: [
            [
              {
                id: 1010101,
                index: 1,
                title: '[塑形健身]',
                desc: ' 完美搭配，你的私人 理疗顾问，搭配发货只为你的健康',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E5%87%8F%E8%82%A5%E9%A4%90/45d81988-8022-40ce-bc8a-5aa013540832.png'
              },
              {
                id: 1010102,
                index: 2,
                title: '[塑形健身]',
                desc: '完美搭配，你的私人 理疗顾问，搭配发货只为你的健康',
                price: '部小果',
                price_del: '',
                commodityType: '',
                imgSrc: 'http://days-matter-1253438854.file.myqcloud.com/fruittribes/products/%E5%87%8F%E8%82%A5%E9%A4%90/fbef6271-5926-44f4-a9b0-9a5e5d7d74e3.png'
              }
            ],
          ]
        }
      ]
    }
  },

  // 搜索页面跳回
  onLoad: function (options) {
    if (options && options.searchValue){
      this.setData({
        searchValue: "搜索："+options.searchValue
      });
    }
  },

  // 搜索入口  
  wxSearchTab: function () {
    wx.redirectTo({
      url: '../search/search'
    })
  }
})
